package com.amplify.simple_android_app;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import org.fest.assertions.api.ANDROID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowNotification;
import org.robolectric.shadows.ShadowNotificationManager;
import org.robolectric.util.ActivityController;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.robolectric.Robolectric.buildActivity;
import static org.robolectric.Robolectric.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class SimpleAndroidActivityTest {

    private ActivityController<SimpleAndroidActivity> controller;

    @Before
    public void setup(){
        controller = buildActivity(SimpleAndroidActivity.class);
    }

    @Test
    public void shouldDisplayHelloWorld() {
        SimpleAndroidActivity activity = controller.create().get();

        TextView textView = (TextView) activity.findViewById(R.id.hello_world);

        ANDROID.assertThat(textView).containsText("Hello World, SimpleAndroidActivity");
    }

    @Test
    public void shouldChangeHelloWorldTextOnButtonPress() {
        SimpleAndroidActivity activity = controller.create().get();

        Button boomButton = (Button) activity.findViewById(R.id.boom_button);
        boomButton.performClick();

        TextView helloWorldView = (TextView) activity.findViewById(R.id.hello_world);

        String expectedBoomNoise = activity.getResources().getString(R.string.boom_noise);
        ANDROID.assertThat(helloWorldView).hasText(expectedBoomNoise);
    }

    @Test
    public void shouldSendSingleNotificationOnClick() {
        SimpleAndroidActivity activity = controller.create().get();

        activity.findViewById(R.id.boom_button).performClick();

        NotificationManager notificationManager =
                (NotificationManager) Robolectric.application.getSystemService(Context.NOTIFICATION_SERVICE);

        int expectedNotificationsCount = shadowOf(notificationManager).getAllNotifications().size();
        assertThat(expectedNotificationsCount).isEqualTo(1);
    }

    @Test
    public void shouldPopulateNotificationWithBoomNoises() {
        SimpleAndroidActivity activity = controller.create().get();

        activity.findViewById(R.id.boom_button).performClick();

        ShadowNotificationManager notificationManager =
                shadowOf((NotificationManager) Robolectric.application.getSystemService(Context.NOTIFICATION_SERVICE));

        ShadowNotification notification = shadowOf(notificationManager.getNotification("tag", 101));

        String boomNoise = activity.getResources().getString(R.string.boom_noise);
        assertThat(notification.getContentText()).isEqualTo(boomNoise);
        assertThat(notification.getContentTitle()).isEqualTo(boomNoise);
        assertThat(notification.getSmallIcon()).isEqualTo(R.drawable.ic_launcher);
    }

    @Test
    public void shouldStartNewActivityOnBoomClick() {
        SimpleAndroidActivity activity = controller.create().get();
        Intent expectedIntent = new Intent(activity, BoomActivity.class);

        activity.findViewById(R.id.boom_button).performClick();

        assertThat(Robolectric.getShadowApplication().getNextStartedActivity()).isEqualTo(expectedIntent);
    }
}
