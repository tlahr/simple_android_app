package com.amplify.simple_android_app;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;

@ContentView(R.layout.main)
public class SimpleAndroidActivity extends RoboActivity {

    @InjectView(R.id.hello_world)
    TextView helloWorldView;
    @InjectView(R.id.boom_button)
    Button boomButton;
    @InjectResource(R.string.boom_noise)
    String boomNoise;
    @Inject
    NotificationManager notificationManager;

    /*
      Other cool stuff:
      @InjectExtra("extra_stuff") // inject intent extras w/ defaults
      @InjectPreference           // access android prefs
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helloWorldView.setText(boomNoise);

                Notification notification =
                        new Notification.Builder(SimpleAndroidActivity.this)
                        .setContentTitle(boomNoise)
                        .setContentText(boomNoise)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .build();

                notificationManager.notify("tag", 101, notification);

                Intent intent = new Intent(SimpleAndroidActivity.this, BoomActivity.class);
                startActivity(intent);
            }
        });
    }
}
